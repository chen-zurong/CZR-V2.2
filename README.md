# 基于ESP32的3D打印机主板CZR-V2.2

#### 介绍
该项目使用国产芯片ESP32来完成3D打印机主板的功能设计，旨在降低3D打印机主板的成本，对于广大3D打印机爱好者来说是非常友好，项目固件跑的是Marlin2.0，基于开源固件，所以可以很好的进行后期DIY设计。
做支持的功能介绍:
    1.  支持12~24V DC输入。使用24V电源需要注意外设的匹配，例如热床，加热棒也要使用24V的。
    2.  拥有5轴电机输出，6个电机接口，其中Z轴并联出2个接口，最多可以支持3Z轴设计的3D打印机。
    3.  使用可更换电机驱动设计，大家可以根据自己的需要更换驱动器。
    4.  板载大功率MOS管，在保证散热的情况下最高支持300W功率，一般的加热棒热床都没问题。
    5.  最具有特色的是在主板上集成了OLED显示器和编码器旋钮，不需要额外的屏幕连接。(又节省了一笔开支)
    6.  板载USB转TTL模块，可以直接连接电脑进行下载程序，在线打印的通信需求。
    7.  得益于ESP32自带WIFI功能，所以可以直接通过手机/电脑连接进行相关操作，非常方便（如果内网穿透一手就可以实现远程控制打印）。

#### 软件架构
该项目使用流行开源3D打印机固件Marlin2.0。


#### 安装刷固件软件以及刷固件教程

1.  首先安装代码编辑软件VS Code。
2.  在VS Code中安装汉化插件--Chinese以及固件编译软件--PlatformIO IDE。
3.  在本仓库提供的Marlin2.0固件文件夹中，选择使用VS Code打开编辑。
4.  点击左下角编译按键，完成后上传固件（使用本固件的话，基本功能已经改好了，只需要改一下基本信息编译下载进入你的板子即可使用。）。

#### CZR-V2.2使用说明

1.  通过本仓库库提供的打板文件，到嘉立创下单打样，然后更具BOM表采购元器件，然后对照Image文件夹图片在对应的地方焊接元器件。
2.  板子制作完成后先不安装驱动，上电测试各个地方的供电是否正常，正常后在连接上驱动和电机测试。
3.  使用VS Code打开本仓库提供的Marlin2.0固件。
4.  将#define SERIAL_PORT_2开启用于wifi通信。
5.  在#define MOTHERBOARD后面使用BOARD_CZR_V2。（如果制作CZR-V1.3就选择BOARD_CZR_V1）
6.  #define CZR_Board_OLED开启CZR-V2.2的板载屏幕。
7.  其他的地方和普通Marlin设置一样，网上资料很多，这边就不赘述了。
8.  值得注意的是，如果编译固件的时候报关于-->U8glib-HAL.h: No such file or directory的错误的话，只要打开#define CZR_Board_OLED的注释再编译，通过后再取消注释编译（一直重复知道编译通过）。
9.  首次通过wifi连接到CZR-V2.2的时候需要上传WEB文件，即hirmware文件夹下的ESP3D-WEBUI-2.1文件夹中的index.html.gz。上传后即可使用网页控制CZR-V2.2了。
10. 还有什么问题的话加我的qq群寻求帮助哦！群qq:670727993。备注Gitee上来的即可通过。

#### 群qq:670727993。备注Gitee上来的即可通过。


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
